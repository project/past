<?php

namespace Drupal\past\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Utility\Error;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * An EventSubscriber that listens to exceptions and shutdowns in order to log
 * them.
 */
class PastSubscriber implements EventSubscriberInterface {
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::EXCEPTION][] = ['onKernelException'];
    $events[KernelEvents::REQUEST][] = ['registerShutdownFunction'];
    return $events;
  }

  /**
   * Registers _past_shutdown_function as shutdown function.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Is given by the event dispatcher.
   */
  public function registerShutdownFunction(RequestEvent $event) {
    drupal_register_shutdown_function('_past_shutdown_function');
  }

  /**
   * Logs an exception with the Past backend.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   Is given by the event dispatcher.
   */
  public function onKernelException(ExceptionEvent $event) {
    if (!\Drupal::config('past.settings')->get('exception_handling')) {
      return;
    }
    // Do not log 404 and 403 exceptions.
    if ($event->getThrowable() instanceof HttpException) {
      return;
    }
    try{
      $past_event = past_event_create('past', 'unhandled_exception', $event->getThrowable()->getMessage());
      $past_event->addArgument('exception', $event->getThrowable());
      $past_event->setSeverity(RfcLogLevel::ERROR);
      $past_event->save();
    }
    catch (\Exception $exception2) {
      // Another uncaught exception was thrown while handling the first one.
      // If we are displaying errors, then do so with no possibility of a
      // further uncaught exception being thrown.
      if (error_displayable()) {
        print '<h1>Additional uncaught exception thrown while handling exception.</h1>';
        print '<h2>Original</h2><p>' . Error::renderExceptionSafe($event->getThrowable()) . '</p>';
        print '<h2>Additional</h2><p>' . Error::renderExceptionSafe($exception2) . '</p><hr />';
      }
    }
  }

}
