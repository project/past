<?php

namespace Drupal\Tests\past_form\Functional;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\past\PastEventInterface;
use Drupal\past_db\Entity\PastEvent;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\past\Traits\PastEventTestTrait;

/**
 * Generic Form tests using the database backend.
 *
 * @group past
 */
class PastFormTest extends BrowserTestBase {

  use PastEventTestTrait;

  /**
   * Past form settings configuration.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $config;

  /**
   * Expected Event.
   *
   * @var \Drupal\past\PastEventInterface
   */
  protected $eventToBe;

  /**
   * A user with the 'view past reports' permission.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $viewUser;

  protected static $modules = [
    'past',
    'past_db',
    'past_form',
    'past_testhidden',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->viewUser = $this->drupalCreateUser([
      'view past reports',
      'access site reports',
    ]);
    $this->config = \Drupal::configFactory()->getEditable('past_form.settings');
    $this->config->set('past_form_log_form_ids', ['*'])->save();

    // The test relies on the login form submission being logged.
    $this->useOneTimeLoginLinks = FALSE;
  }

  /**
   * Tests an empty submit handler array.
   */
  public function testFormEmptyArray() {
    // Given a simple form with one button with empty #submit array.
    // Submit the global submit button.
    // Check if the default form handler is executed.
    // Check the logs if the submission is captured.
    $edit = [];
    $form_id = 'past_testhidden_form_empty_submit_array';
    $button_value = 'Submit';
    $this->drupalGet($form_id);
    $this->assertSession()->pageTextContains('form handler called by ' . $form_id);
    $this->submitForm($edit, $button_value);
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value);
  }

  /**
   * Tests the exclusion of views exposed form submission.
   */
  public function testViewsExposedForm() {
    // Check that views_exposed_filter is not recorded.
    $admin = $this->drupalCreateUser(['view past reports']);
    $this->drupalLogin($admin);

    // The login event is logged.
    $event = $this->getLastEventByMachineName('submit');
    $this->assertTrue("Form submitted: user_login_form, Log in" == $event->getMessage(), 'user_login submit was not logged.');

    // First check the fake exclude for views with exposed filters.
    $this->drupalGet('admin/reports/past');

    // The last event is still the login.
    $event = $this->getLastEventByMachineName('submit');
    $this->assertTrue("Form submitted: user_login_form, Log in" == $event->getMessage(), 'views_exposed_form submit was not logged.');
    $this->drupalGet('admin/reports/past');

    // Additional submits after the page load submit.
    $this->submitForm(['module' => 'watchdog'], t('Apply'));
    $event = $this->getLastEventByMachineName('submit');
    $this->assertTrue("Form submitted: user_login_form, Log in" == $event->getMessage(), 'views_exposed_form submit was not logged.');
    // @todo This should add the submission. Wrong currently!
  }

  /**
   * Tests a form without submit handler.
   */
  public function testFormNoSubmitHandler() {
    // Given a simple form with one button with default #submit function entry.
    // Same as above.
    $edit = [];
    $form_id = 'past_testhidden_form_default_submit_handler';
    $button_value = 'Submit';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $this->submitForm($edit, $button_value);
    $this->assertSession()->pageTextContains('global submit handler called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);
  }

  /**
   * Tests a forms with global/custom submit hanlder.
   */
  public function testFormCustomSubmithandler() {
    // Given a simple form with one button with special #submit function..
    // Submit a form with global submit handler.
    // Check if the form handler is executed.
    // Check the logs if the submission is captured.
    $edit = [];
    $form_id = 'past_testhidden_form_custom_submit_handler';
    $button_value = 'Submit';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $this->submitForm($edit, $button_value);
    $this->assertSession()->pageTextContains('custom submit handler called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);

    $edit = [];
    $form_id = 'past_testhidden_form_mixed_submit_handlers';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $this->submitForm($edit, $button_value);
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);
    $this->assertSession()->pageTextContains('submit handler called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);
  }

  /**
   * Tests a form with several submits.
   */
  public function testFormThreeButtons() {
    // Given a simple form with three buttons without specific handlers
    // Submit each button.
    // Check the logs if the submissions where captured.
    // Check the logs which button was pressed each.
    $edit = [];
    $form_id = 'past_testhidden_form_three_buttons';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $button_value = 'Button 1';
    $this->submitForm($edit, $button_value);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
      'submit_one' => 'Button 1',
      'submit_two' => 'Button 2',
      'submit_three' => 'Button 3',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);

    $button_value = 'Button 2';
    $this->submitForm($edit, $button_value);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
      'submit_one' => 'Button 1',
      'submit_two' => 'Button 2',
      'submit_three' => 'Button 3',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);

    $button_value = 'Button 3';
    $this->submitForm($edit, $button_value);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
      'submit_one' => 'Button 1',
      'submit_two' => 'Button 2',
      'submit_three' => 'Button 3',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);
  }

  /**
   * Tests a form with several submits having submit handlers.
   */
  public function testFormThreeButtonsWithHandlers() {
    // Given a simple form with three buttons.
    // Each of the buttons has an own #submit handler.
    // Submit each button.
    // Check if each specific handler is executed.
    // Check the global handler was not executed.
    // Check the logs if the submissions where captured.
    $edit = [];
    $form_id = 'past_testhidden_form_three_buttons_with_submit_handlers';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $button_value = 'Button 1';
    $this->submitForm($edit, $button_value);
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);
    $this->assertSession()->pageTextContains('custom submit handler ' . $button_value . ' called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
      'submit_one' => 'Button 1',
      'submit_two' => 'Button 2',
      'submit_three' => 'Button 3',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);

    $button_value = 'Button 2';
    $this->submitForm($edit, $button_value);
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);
    $this->assertSession()->pageTextContains('custom submit handler ' . $button_value . ' called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
      'submit_one' => 'Button 1',
      'submit_two' => 'Button 2',
      'submit_three' => 'Button 3',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);

    $button_value = 'Button 3';
    $this->submitForm($edit, $button_value);
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);
    $this->assertSession()->pageTextContains('custom submit handler ' . $button_value . ' called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
      'submit_one' => 'Button 1',
      'submit_two' => 'Button 2',
      'submit_three' => 'Button 3',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);
  }

  /**
   * Tests a form with type=button submits having submit handlers.
   */
  public function testFormUseButtonAsSubmit() {
    // Capture #type=button submissions.
    $edit = [];
    $form_id = 'past_testhidden_form_normal_button';
    $button_value = 'Button';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $this->submitForm($edit, $button_value);
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);
    $this->assertSession()->responseNotContains('custom submit handler called by ' . $form_id);

    $this->assertNull($this->getLastEventByMachineName('submit'));

    // Is logged if we use '#executes_submit_callback' => TRUE.
    $button_value = 'Submittable';
    $this->submitForm($edit, $button_value);
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);
    $this->assertSession()->pageTextContains('custom submit handler called by ' . $form_id);
    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
      'submittable' => 'Submittable',
      'button' => 'Button',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);
  }

  /**
   * Tests a form validation.
   */
  public function testFormValidation() {
    // Enable validation logging.
    $this->config->set('past_form_log_validations', TRUE)->save();
    // Capture case for a failing validations.
    // Capture validation error message in the past log.
    $edit = ['sample_property' => ''];
    $form_id = 'past_testhidden_form_custom_submit_handler';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);
    $this->assertSession()->elementExists('css', 'input.error');

    $this->assertNull($this->getLastEventByMachineName('submit'));

    // Enable validation logging.
    $this->config->set('past_form_log_validations', 1)->save();

    $edit = ['sample_property' => ''];
    $form_id = 'past_testhidden_form_multi_validation';
    $button_value = 'Submit';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $this->submitForm($edit, $button_value);
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);
    $this->assertSession()->elementExists('css', 'input.error');

    $event = $this->getLastEventByMachineName('validate');
    $this->eventToBe = $this->getEventToBe('validate', $form_id, $button_value, 'Form validation error:');
    $errors_to_be = [
      'sample_property' => [
        'message' => 'Sample Property field is required.',
        'submitted' => '',
      ],
      'another_sample_property' => [
        'message' => 'Another Sample Property field is required.',
        'submitted' => 0,
      ],
      'sample_select' => [
        'message' => 'Sample Select: says, don\'t be a maybe ..',
        'submitted' => '2',
      ],
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, [], $errors_to_be);

    $edit = [];
    $form_id = 'past_testhidden_form_custom_validation_only';
    $button_value = 'Submit';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $this->submitForm($edit, $button_value);
    $this->assertSession()->responseNotContains('global submit handler called by ' . $form_id);
    $element = $this->assertSession()->elementExists('css', 'select.error');
    $this->assertEquals('edit-sample-select', $element->getAttribute('id'));

    $event = $this->getLastEventByMachineName('validate');
    $this->eventToBe = $this->getEventToBe('validate', $form_id, $button_value, 'Form validation error:');
    $errors_to_be = [
      'sample_select' => [
        'message' => 'Sample Select: says, don\'t be a maybe ..',
        'submitted' => '2',
      ],
    ];
    // @todo wrong!
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, [], $errors_to_be);

    // Test validation logging of nested form elements.
    $form_id = 'past_testhidden_form_nested';
    $button_value = t('Submit');
    $this->drupalGet($form_id);
    $edit = ['wrapper[field_1]' => 'wrong value'];
    $this->submitForm($edit, $button_value);
    // Check for correct validation error messages and CSS class on field.
    $this->assertSession()->pageTextContains(t("Field 1 doesn't contain the right value"));
    $element = $this->assertSession()->elementExists('css', 'input.error');
    $this->assertEquals('edit-wrapper-field-1', $element->getAttribute('id'));
    // Load latest validation log record and create an artificial that contains
    // exactly what a correct logging is supposed to contain.
    $event = $this->getLastEventByMachineName('validate');
    $this->eventToBe = $this->getEventToBe('validate', $form_id, $button_value, 'Form validation error:');
    $errors_to_be = [
      'wrapper][field_1' => [
        'message' => t("Field 1 doesn't contain the right value"),
        'submitted' => $edit['wrapper[field_1]'],
      ],
    ];
    // Compare artificial event with logged event.
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, [], $errors_to_be, 'Validation of nested elements was logged correctly.');

    // Test if submission is logged as expected.
    $edit = [
      'wrapper[field_1]' => 'correct value',
      'wrapper[field_2]' => 'some other value',
    ];
    $this->submitForm($edit, $button_value);
    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'wrapper' => [
        'field_1' => $edit['wrapper[field_1]'],
        'field_2' => $edit['wrapper[field_2]'],
      ],
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be, [], 'Submission of nested elements was logged correctly');
  }

  /**
   * Tests a multi step form.
   */
  public function testFormMultistep() {
    // Capture multistep submissions.
    $edit = [];
    $form_id = 'past_testhidden_form_multistep';
    $button_value = 'Next';
    $step = 1;
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler step ' . $step . ' called by ' . $form_id);
    $this->submitForm($edit, $button_value);
    $this->assertSession()->pageTextContains('global submit handler step ' . $step . ' called by ' . $form_id);
    $step++;
    $this->assertSession()->responseContains('form handler step ' . $step . ' called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
      'next' => 'Next',
    ];

    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);

    $this->submitForm($edit, $button_value);
    $this->assertSession()->pageTextContains('global submit handler step ' . $step . ' called by ' . $form_id);
    $step++;
    $this->assertSession()->responseContains('form handler step ' . $step . ' called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property_2' => 'sample value 2',
      'next' => 'Next',
      'back' => 'Back',
    ];

    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);

    $button_value = 'Back';
    $this->submitForm($edit, $button_value);
    $this->assertSession()->pageTextContains('global submit handler step ' . $step . ' called by ' . $form_id);
    $step--;
    $this->assertSession()->responseContains('form handler step ' . $step . ' called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'back' => 'Back',
    ];

    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);

    $button_value = 'Next';
    $this->submitForm($edit, $button_value);
    $this->assertSession()->pageTextContains('global submit handler step ' . $step . ' called by ' . $form_id);
    $step++;
    $this->assertSession()->responseContains('form handler step ' . $step . ' called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'back' => 'Back',
    ];

    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);

    $button_value = 'Submit';
    $this->submitForm($edit, $button_value);
    $this->assertSession()->pageTextContains('global submit handler step ' . $step . ' called by ' . $form_id);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property_3' => 'sample value 3',
      'submit' => $button_value,
    ];

    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);
  }

  /**
   * Tests the include filter.
   */
  public function testFormIdFilter() {
    $edit = [];
    $form_id = 'past_testhidden_form_empty_submit_array';
    $button_value = 'Submit';

    // Test exclusion.
    $this->config->set('past_form_log_form_ids', [''])->save();
    $this->drupalGet($form_id);
    $this->submitForm($edit, $button_value);

    // Event shouldn't be logged.
    $this->assertNull($this->getLastEventByMachineName('submit'));

    // Test inclusion.
    $this->config->set('past_form_log_form_ids', [$form_id])->save();
    $this->drupalGet($form_id);
    $this->submitForm($edit, $button_value);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value);

    $form_id = 'past_testhidden_form_custom_submit_handler';
    $this->drupalGet($form_id);
    $this->submitForm($edit, $button_value);

    // This new event should not be logged, so still the old event should be
    // fetched.
    $event = $this->getLastEventByMachineName('submit');
    $this->assertSameEvent($event, $this->eventToBe, 'past_testhidden_form_empty_submit_array', $button_value);

    // Test inclusion of more then one form_id.
    $this->config
      ->set('past_form_log_form_ids', array_merge($this->config->get('past_form_log_form_ids'), [$form_id]))
      ->save();
    $this->drupalGet($form_id);
    $this->submitForm($edit, $button_value);

    // Now the new event should be found.
    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value);

    // Test wildcard.
    $this->config->set('past_form_log_form_ids', ['*testhidden_form_*'])->save();
    $form_id = 'past_testhidden_form_empty_submit_array';
    $this->drupalGet($form_id);
    $this->submitForm($edit, $button_value);

    // Again the first form_id should be found.
    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value);

    $form_id = 'past_testhidden_form_custom_submit_handler';
    $this->drupalGet($form_id);
    $this->submitForm($edit, $button_value);

    // And also the new one.
    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value);

    // Check if user registration is not logged.
    $this->drupalGet('user/register');
    $register_edit = [
      'name' => $this->randomMachineName(),
      'mail' => $this->randomMachineName() . '@example.com',
    ];
    $register_button_value = t('Create new account');
    $this->submitForm($register_edit, $register_button_value);

    // Load last logged submission and check whether it's not the user register
    // submission.
    $event = $this->getLastEventByMachineName('submit');
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $edit);

    // Test wildcard that will match all forms.
    $this->config->set('past_form_log_form_ids', ['*'])->save();
    $form_id = 'past_testhidden_form_empty_submit_array';
    $this->drupalGet($form_id);
    $this->submitForm($edit, $button_value);

    // Again the first form_id should be found.
    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value);

    $form_id = 'past_testhidden_form_custom_submit_handler';
    $this->drupalGet($form_id);
    $this->submitForm($edit, $button_value);

    // And also the new one.
    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value);

    // Check if user registration is logged.
    $form_id = 'user_register_form';
    $register_edit = [
      'name' => $this->randomMachineName(),
      'mail' => $this->randomMachineName() . '@example.com',
    ];
    $this->drupalGet('user/register');
    $this->submitForm($register_edit, $register_button_value);

    // Check if event was logged.
    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $register_button_value);
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $register_button_value, $register_edit);

    // Test that additional spaces and special characters don't confuse the form
    // ID check.
    $form_id = 'past_testhidden_form_empty_submit_array';
    $this->config->set('past_form_log_form_ids', [$form_id, 'other_form'])->save();
    $this->drupalGet($form_id);
    $this->submitForm($edit, $button_value);

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value);
  }

  /**
   * Tests the Past event log from UI.
   */
  public function testAdminUi() {
    $this->drupalLogin($this->viewUser);
    $this->createFormEvents();

    // Check that just the form events are displayed.
    $this->drupalGet('admin/reports/past/form');
    $this->assertSession()->pageTextNotContains('Event message #3');

    // Check filters in Past event log.
    $this->assertEquals(1, count($this->xpath('//tbody/tr/td[3][contains(., "form1")]')), 'Filtered by Form ID.');
    $this->assertEquals(4, count($this->xpath('//tbody/tr/td[4][contains(., "operation0")]')), 'Filtered by Operation.');
    $this->drupalGet('admin/reports/past/form', [
      'query' => [
        'argument_data' => 'form2',
      ],
    ]);
    $this->assertEquals(1, count($this->xpath('//tbody/tr/td[3][contains(., "form2")]')), 'Filtered by Form ID.');
    $this->assertEquals(0, count($this->xpath('//tbody/tr/td[3][contains(., "form1")]')), 'Filtered by Form ID.');

    $this->drupalGet('admin/reports/past/form', [
      'query' => [
        'argument_data_1' => 'operation1',
      ],
    ]);
    $this->assertEquals(4, count($this->xpath('//tbody/tr/td[4][contains(., "operation1")]')), 'Filtered by Operation.');
    $this->assertEquals(0, count($this->xpath('//tbody/tr/td[4][contains(., "operation0")]')), 'Filtered by Operation.');

  }

  /**
   * Creates some sample form events.
   */
  protected function createFormEvents($count = 10) {
    // Prepare some logs.
    $events = [];
    for ($i = 1; $i < $count; $i++) {
      if ($i % 10 == 3) {
        $event = past_event_create('no_past_form_event', $this->randomMachineName(), 'Event message #' . $i);
      }
      else {
        $event = past_event_create('past_form', $this->randomMachineName(), 'Form message #' . $i);
      }
      $event->setReferer('http://example.com/test-referer');
      $event->setLocation('http://example.com/test-location');
      $event->addArgument('form_id', 'form' . ($i % 10));
      $event->addArgument('operation', 'operation' . ($i % 2));
      $event->setSeverity(RfcLogLevel::DEBUG);
      $event->save();
      $events[] = $event;
    }
    return $events;
  }

}
