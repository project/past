<?php

namespace Drupal\Tests\past_form\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\past\Traits\PastEventTestTrait;

/**
 * Generic Form tests using the database backend.
 *
 * @group past
 */
class PastFormJavascriptTest extends WebDriverTestBase {

  use PastEventTestTrait;

  /**
   * Past form settings configuration.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $config;

  /**
   * Expected Event.
   *
   * @var \Drupal\past\PastEventInterface
   */
  protected $eventToBe;

  /**
   * A user with the 'view past reports' permission.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $viewUser;

  protected static $modules = [
    'past',
    'past_db',
    'past_form',
    'past_testhidden',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->viewUser = $this->drupalCreateUser([
      'view past reports',
      'access site reports',
    ]);
    $this->config = \Drupal::configFactory()->getEditable('past_form.settings');
    $this->config->set('past_form_log_form_ids', ['*'])->save();
  }

  /**
   * Tests an ajax form.
   */
  public function testFormAjax() {
    // Capture ajax submissions.
    $edit = [];
    $form_id = 'past_testhidden_form_simple_ajax';
    $button_value = 'Submit';
    $this->drupalGet($form_id);
    $this->assertSession()->responseContains('form handler called by ' . $form_id);
    $this->getSession()->getPage()->pressButton('Submit');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->pageTextNotContains('global submit handler called by ' . $form_id);
    $this->assertSession()->pageTextContains('custom submit handler called by ' . $form_id);
    $this->assertSession()->pageTextContains('ajax called by ' . $form_id . ' with sample_property containing: sample value');

    $event = $this->getLastEventByMachineName('submit');
    $this->eventToBe = $this->getEventToBe('submit', $form_id, $button_value);
    $values_to_be = [
      'sample_property' => 'sample value',
    ];
    $this->assertSameEvent($event, $this->eventToBe, $form_id, $button_value, $values_to_be);
  }

}
