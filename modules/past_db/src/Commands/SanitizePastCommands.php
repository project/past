<?php

namespace Drupal\past_db\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\sql\SanitizePluginInterface;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Drush sql-sanitize plugin for sanitizing (truncating) past entries.
 */
class SanitizePastCommands extends DrushCommands implements SanitizePluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SanitizePastCommands constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $database, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->database = $database;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sanitize past entries from the DB.
   *
   * @hook post-command sql-sanitize
   *
   * {@inheritdoc}
   */
  public function sanitize($result, CommandData $command_data) {
    $options = $command_data->options();
    if ($this->isEnabled($options['sanitize-past'])) {
      $this->database->truncate('past_event')->execute();
      $this->database->truncate('past_event_data')->execute();
      $this->database->truncate('past_event_argument')->execute();
      $this->entityTypeManager->getStorage('past_event')->resetCache();
      $this->logger()->notice(dt('Past tables truncated.'));
    }
  }

  /**
   * Sanitization options.
   *
   * @hook option sql-sanitize
   * @option sanitize-past
   *   By default, past log entries are truncated. Specify 'no' to disable that.
   */
  public function options($options = ['sanitize-past' => NULL]) {}

  /**
   * Sanitization messages.
   *
   * @hook on-event sql-sanitize-confirms
   *
   * {@inheritdoc}
   */
  public function messages(&$messages, InputInterface $input) {
    $options = $input->getOptions();
    if ($this->isEnabled($options['sanitize-past'])) {
      $messages[] = dt('Truncate past entries tables.');
    }
  }

  /**
   * Test an option value to see if it is disabled.
   *
   * @param string $value
   *   The enabled options value.
   *
   * @return bool
   *   TRUE if santize past is enabled.
   */
  protected function isEnabled($value) {
    return ($value !== 'no');
  }

}
