<?php

namespace Drupal\past_db;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Url;
use Drupal\past_db\Entity\PastEvent;

/**
 * Render controller for taxonomy terms.
 */
class PastEventViewBuilder extends EntityViewBuilder {
  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    parent::buildComponents($build, $entities, $displays, $view_mode);

    foreach ($entities as $id => $entity) {
      /** @var PastEvent $entity */

      // Global information about the event.
      $build[$id]['uid'] = [
        '#type' => 'item',
        '#title' => t('Actor'),
      ];
      $build[$id]['uid'][] = $entity->getActorDropbutton(FALSE);

      // Output URLs as links.
      if ($entity->getReferer()) {
        try {
          $referer = \Drupal::service('link_generator')->generate($entity->getReferer(), Url::fromUri($entity->getReferer()));
        }
        catch (\InvalidArgumentException $e) {
          // If referer is not a regular URL (e.g. mobile app).
          // Display it as just as a string.
          $referer = '<em>' . $entity->getReferer() . '</em>';
        }
        $build[$id]['referer'][0] = [
          '#markup' => $referer,
        ];
      }
      if ($entity->getLocation()) {
        $build[$id]['location'][0] = [
          '#markup' => \Drupal::service('link_generator')->generate($entity->getLocation(), Url::fromUri($entity->getLocation())),
        ];
      }

      // @todo Display as vertical_tabs if that is enabled outside forms.
      foreach ($entity->getArguments() as $key => $argument) {
        $build[$id]['arguments']['fieldset_' . $key] = $entity->formatArgument($key, $argument);
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    // There is no template, unset it to avoid a watchdog notice.
    unset($build['#theme']);
    return $build;
  }

}
