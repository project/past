<?php

/**
 * @file
 * Installation hooks.
 */

/**
 * Implements hook_schema().
 */
function past_db_schema() {
  $schema['past_event_argument'] = [
    'description' => 'An event argument',
    'fields' => [
      'argument_id' => [
        'description' => 'The identifier of the event argument.',
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'event_id' => [
        'description' => 'The identifier of the event.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'name' => [
        'description' => 'The name of this argument',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ],
      'type' => [
        'description' => 'The type of this argument',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ],
      'raw' => [
        'description' => 'The raw data of this argument',
        'type' => 'text',
      ],
    ],
    'primary key' => ['argument_id'],
    'indexes' => [
      'event_id' => ['event_id'],
    ],
  ];
  $schema['past_event_data'] = [
    'description' => 'An event argument',
    'fields' => [
      'data_id' => [
        'description' => 'The identifier of the data item',
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'argument_id' => [
        'description' => 'The identifier of the event argument.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'parent_data_id' => [
        'description' => 'The identifier of the parent data item.',
        'type' => 'int',
      ],
      'serialized' => [
        'description' => 'If the value is serialized or not.',
        'type' => 'int',
        'size' => 'tiny',
      ],
      'name' => [
        'description' => 'The name of this data item',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ],
      'type' => [
        'description' => 'The type of this argument',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ],
      'value' => [
        'description' => 'The value of this data item',
        'type' => 'text',
        'size' => 'medium',
      ],
    ],
    'primary key' => ['data_id'],
    'indexes' => [
      'argument_id' => ['argument_id'],
    ],
  ];

  return $schema;
}
