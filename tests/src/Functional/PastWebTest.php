<?php

namespace Drupal\Tests\past\Functional;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\past\Traits\PastEventTestTrait;

/**
 * Generic API web tests using the database backend.
 *
 * @group past
 */
class PastWebTest extends BrowserTestBase {

  use PastEventTestTrait;

  /**
   * Modules required to run the tests.
   *
   * @var string[]
   */
  protected static $modules = [
    'past',
    'past_db',
    'past_testhidden',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    // Empty the logfile, our fatal errors are expected.
    $filename = DRUPAL_ROOT . '/' . $this->siteDirectory . '/error.log';
    file_put_contents($filename, '');
    parent::tearDown();
  }

  /**
   * Tests the disabled exception handler.
   */
  public function testExceptionHandler() {
    // Create user to test logged uid.
    $account = $this->drupalCreateUser();
    $this->drupalLogin($account);

    // Let's produce an exception, the exception handler is enabled by default.
    $this->drupalGet('past_trigger_error/Exception');
    $this->assertSession()->pageTextContains(t('The website encountered an unexpected error. Try again later.'));
    $this->assertSession()->pageTextContains('Exception: This is an exception.');

    // Now we should have a log event, assert it.
    $event = $this->getLastEventByMachinename('unhandled_exception');
    $this->assertEquals('past', $event->getModule());
    $this->assertEquals('unhandled_exception', $event->getMachineName());
    $this->assertEquals(RfcLogLevel::ERROR, $event->getSeverity());
    $this->assertEquals(1, count($event->getArguments()));

    $this->drupalGet('test');
    // Test for not displaying 403 and 404 logs.
    $event_404 = $this->getLastEventByMachinename('unhandled_exception');
    $this->assertEquals($event->id(), $event_404->id(), 'No 403 and 404 logs were displayed');

    $data = $event->getArgument('exception')->getData();
    $this->assertTrue(array_key_exists('backtrace', $data));
    $this->assertEquals($account->id(), $event->getUid());

    // Disable exception handling and re-throw the exception.
    $this->config('past.settings')
      ->set('exception_handling', 0)
      ->save();
    $this->drupalGet('past_trigger_error/Exception');
    $this->assertSession()->pageTextContains(t('The website encountered an unexpected error. Try again later.'));
    $this->assertSession()->pageTextContains('Exception: This is an exception.');

    // No new exception should have been logged.
    $event_2 = $this->getLastEventByMachinename('unhandled_exception');
    $this->assertEquals($event->id(), $event_2->id(), 'No new event was logged');
  }

  /**
   * Tests the shutdown function.
   */
  public function testShutdownFunction() {
    $this->markTestSkipped('Skipped because it is not possible to suppress errors');
    // Enable hook_watchdog capture.
    $this->config('past.settings')
      ->set('log_watchdog', 1)
      ->save();

    // Create user to test logged uid.
    $account = $this->drupalCreateUser();
    $this->drupalLogin($account);

    // Let's trigger an error, the error handler is disabled by default.
    $this->drupalGet('past_trigger_error/E_ERROR');

    // Now we have a log event, assert it.
    // In PHP 5, this is logged as a fatal error. In PHP 7, this is catched by
    // the standard exception handler and logged through watchdog.
    // @todo: Improve our exception handling to catch this with a backtrace in
    //   PHP 7.
    if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
      $event = $this->getLastEventByMachinename('php');
      $this->assertEquals('watchdog', $event->getModule());
      $this->assertEquals(RfcLogLevel::ERROR, $event->getSeverity());
      $this->assertEquals(1, count($event->getArguments()));
      $this->assertTrue(strpos($event->getMessage(), 'Error: Cannot use object of type stdClass as array') !== FALSE);
    }
    else {
      $event = $this->getLastEventByMachinename('fatal_error');
      $this->assertEquals('past', $event->getModule());
      $this->assertEquals(RfcLogLevel::CRITICAL, $event->getSeverity());
      $this->assertEquals(1, count($event->getArguments()));
      $this->assertEquals('Cannot use object of type stdClass as array', $event->getMessage());
      $data = $event->getArgument('error')->getData();
      $this->assertEquals($data['type'], E_ERROR);
      $this->assertEquals($account->id(), $event->getUid());
    }
  }

  /**
   * Tests triggering PHP errors.
   *
   * @todo We leave out E_PARSE as we can't handle it and it would make our code unclean.
   * We do not test E_USER_* cases. They are not PHP errors.
   */
  public function testErrors() {
    $this->markTestSkipped('Skipped because it is not possible to suppress errors');
    // Enable hook_watchdog capture.
    $this->config('past.settings')
      ->set('log_watchdog', 1)
      ->save();

    $this->drupalGet('past_trigger_error/E_COMPILE_ERROR');
    $event = $this->getLastEventByMachinename('php');
    $this->assertTextContains($event->getMessage(), 'Warning: require_once');

    $this->drupalGet('past_trigger_error/E_COMPILE_WARNING');
    $event = $this->getLastEventByMachinename('php');
    $this->assertTextContains($event->getMessage(), 'Warning: include_once');

    $this->drupalGet('past_trigger_error/E_DEPRECATED');
    $event = $this->getLastEventByMachinename('php');
    if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
      $this->assertTextContains($event->getMessage(), 'Error: Call to undefined function call_user_method()');
    }
    else {
      $this->assertTextContains($event->getMessage(), 'Deprecated function: Function call_user_method() is deprecated');
    }

    $this->drupalGet('past_trigger_error/E_NOTICE');
    $event = $this->getLastEventByMachinename('php');
    $this->assertTextContains($event->getMessage(), 'Notice: Undefined variable');

    $this->drupalGet('past_trigger_error/E_RECOVERABLE_ERROR');
    $event = $this->getLastEventByMachinename('php');
    if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
      $this->assertTextContains($event->getMessage(), 'TypeError: Argument 1 passed to my_func() must be of the type array, null given');
    }
    else {
      $this->assertTextContains($event->getMessage(), 'Recoverable fatal error');
    }

    $this->drupalGet('past_trigger_error/E_WARNING');
    $event = $this->getLastEventByMachinename('php');
    $this->assertTextContains($event->getMessage(), 'Warning: fopen');
  }

  /**
   * Tests the settings form.
   */
  public function testSettings() {
    // Create user.
    $account = $this->drupalCreateUser([
      'administer past',
    ]);
    $this->drupalLogin($account);
    $edit = [
      'log_watchdog' => TRUE,
    ];
    $this->drupalGet('admin/config/development/past');
    $this->submitForm($edit, t('Save configuration'));
    $this->assertSession()->checkboxChecked("edit-backtrace-include-severity-0");
    $this->assertSession()->checkboxNotChecked("edit-backtrace-include-severity-6");
    $edit = [
      'backtrace_include[severity_0]' => FALSE,
    ];
    $this->submitForm($edit, t('Save configuration'));
    $this->assertSession()->checkboxNotChecked("edit-backtrace-include-severity-0");
    $edit = [
      'backtrace_include[severity_0]' => TRUE,
    ];
    $this->submitForm($edit, t('Save configuration'));
    $this->assertSession()->checkboxChecked("edit-backtrace-include-severity-0");
  }

  /**
   * Asserts if $text contains $chunk.
   *
   * @param string $text
   * @param string $chunk
   */
  function assertTextContains($text, $chunk) {
    $this->assertTrue(strpos($text, $chunk) !== FALSE, t('@text contains @chunk.', [
      '@text' => $text,
      '@chunk' => $chunk,
    ]));
  }
  /**
   * Asserts if $text starts with $chunk.
   *
   * @param string $text
   * @param string $chunk
   */
  function assertTextStartsWith($text, $chunk) {
    $this->assertTrue(strpos($text, $chunk) === 0, t('@text starts with @chunk.', [
      '@text' => $text,
      '@chunk' => $chunk
    ]));
  }

  /**
   * Tests the actor dropbutton in the past page handling with deleted user.
   */
  public function testGetActorDropbutton() {
    // Create a new user and create past event.
    $user = $this->drupalCreateUser();
    $event = past_event_create('past', 'test', 'Test Log Entry', [
      'uid' => $user->id(),
    ]);
    $event->save();
    // Delete the user and create/login with another user.
    $user->delete();
    $user2 = $this->drupalCreateUser([
      'view past reports',
      'access site reports',
    ]);
    $this->drupalLogin($user2);
    // Check that a fatal error is not encountered when visiting the past page.
    $this->drupalGet('admin/reports/past');
    $this->assertSession()->statusCodeEquals(200);
    // Check the link redirect to a non-fatal error page.
    $this->getSession()->getPage()->clickLink('Session: ');
    $this->assertSession()->statusCodeEquals(200);
  }

}
