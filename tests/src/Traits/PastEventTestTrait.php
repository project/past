<?php

namespace Drupal\Tests\past\Traits;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\past\PastEventInterface;
use Drupal\past_db\Entity\PastEvent;

/**
 * Provides common helper methods for Past Event tests.
 */
trait PastEventTestTrait {

  /**
   * Returns the last event with a given machine name.
   *
   * @param string $machine_name
   *   Machine name of a past event entity.
   *
   * @return \Drupal\past\PastEventInterface|null
   *   The loaded past event object or null if not found.
   */
  public function getLastEventByMachineName($machine_name) {
    $event_ids = \Drupal::entityQuery('past_event')
      ->accessCheck(FALSE)
      ->condition('machine_name', $machine_name, '=')
      ->sort('event_id', 'DESC')
      ->range(0, 1)
      ->execute();
    if ($event_ids) {
      return \Drupal::entityTypeManager()->getStorage('past_event')->load(reset($event_ids));
    }
    return NULL;
  }

  /**
   * Compares two past_form events.
   *
   * @param PastEventInterface $first
   *   The first comparison object.
   * @param PastEventInterface $second
   *   The second comparison object.
   * @param string $form_id
   *   The machine name of the form.
   * @param string $button_value
   *   The string shown on the submit button.
   * @param array $values
   *   (optional) Values to check on 'values' argument.
   * @param array $errors
   *   (optional) Errors to check on 'errors' argument.
   * @param string|null $message
   *   (optional) A description of the event.
   */
  public function assertSameEvent(PastEventInterface $first, PastEventInterface $second, $form_id, $button_value, array $values = [], array $errors = [], $message = NULL) {
    if (!empty($first)) {
      $mismatch = [];

      if ($first->getModule() != $second->getModule()) {
        $mismatch[] = 'module';
      }
      if ($first->getMachineName() != $second->getMachineName()) {
        $mismatch[] = 'machine_name';
      }
      if ($first->getMessage() != $second->getMessage()) {
        $mismatch[] = 'message';
      }
      if ($first->getSeverity() != $second->getSeverity()) {
        $mismatch[] = 'severity';
      }
      if ($first->getArgument('form_id')->getData() != $form_id) {
        $mismatch[] = 'form_id';
      }
      if ($first->getArgument('operation')->getData() != $button_value) {
        $mismatch[] = 'operation';
      }

      if (!empty($values)) {
        $arg_values = $first->getArgument('values')->getData();
        foreach ($values as $k => $v) {
          if (!isset($arg_values[$k]) || $arg_values[$k] != $v) {
            if (is_array($v)) {
              $mismatch[] = 'values: ' . implode(', ', $v);
            }
            else {
              $mismatch[] = 'value: ' . $v;
            }
          }
        }
      }

      if (!empty($errors)) {
        $arg_errors = $first->getArgument('errors')->getData();
        foreach ($errors as $k => $v) {
          if ($v['message'] instanceof TranslatableMarkup) {
            if (!isset($arg_errors[$k]) || $arg_errors[$k]['message']->render() != $v['message']->render()) {
              $mismatch[] = 'error value: ' . $v['message']->render();
            }
          }
          else {
            if (!isset($arg_errors[$k]) || $arg_errors[$k]['message'] != $v['message']) {
              $mismatch[] = 'error value: ' . $v['message'];
            }
          }
        }
      }

      if (!empty($mismatch)) {
        $this->fail('The following properties do not match: ' . implode(', ', $mismatch));
      }

      $this->assertTrue(empty($mismatch), ($message ? $message : t('Event @first is equal to @second.', ['@first' => $first->getMessage(), '@second' => $second->getMessage()])));
    }
  }


  /**
   * Returns a standard past_form event.
   *
   * @param string $machine_name
   *   The machine name of the event.
   * @param string $form_id
   *   The machine name of the form.
   * @param string $button_value
   *   The string shown on the submit button.
   * @param string $message
   *   (optional) A description of the event.
   *
   * @return \Drupal\past_db\Entity\PastEvent
   *   The event created.
   */
  public function getEventToBe($machine_name, $form_id, $button_value, $message = 'Form submitted:') {
    $event = past_event_create('past_form', $machine_name, $message . ' ' . $form_id . ', ' . $button_value);
    $event->setSeverity(RfcLogLevel::DEBUG);
    return $event;
  }

}
